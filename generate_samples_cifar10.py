from evaluation_lib import *
import pickle

sampler = AllSampler(dataset_name="cifar10")

# 2000 seen examples

for i in range(4):
    data = sampler.sample_all_labels(50) # 10 * 50 * 4 = 2000
    pickle.dump(data, open(f"samples/cifar10_seen_samples_{i}.pkl", "wb"))

# 2000 unseen examples (n=2)
for i in range(4):
    data = sampler.sample_mix_of_vectors(500, 2)
    pickle.dump(data, open(f"samples/cifar10_unseen_samples_2_{i}.pkl", "wb"))

# 2000 unseen examples (n=3)
for i in range(4):
    data = sampler.sample_mix_of_vectors(500, 3)
    pickle.dump(data, open(f"samples/cifar10_unseen_samples_3_{i}.pkl", "wb"))
'''
Implementing the following algorithm:

\begin{algorithm}[ht]
\begin{algorithmic}
   \STATE {\bfseries Input:} datasets $D$, models $M$, number of labels $n$
   \STATE {\bfseries Output:} Cosine similarity scores for each model
   
   \FORALL{dataset $d$ in $D$}
   \FORALL{data point in $d$}
   \STATE Select $n$ random labels $L$ from $d$
   \STATE Generate combined label context $C_{lc}$ from $L$
   \FORALL{model $m$ in $M$}
   \STATE Initialize scores list $S_m$ for model $m$
   \IF{$m$ is CLIP}
   \STATE Use $C_{lc}$ directly for generating image $I$
   \ELSIF{$m$ is Text Embedding}
   \STATE Sum embeddings of each label in $L$ to get $C_{te}$
   \STATE Use $C_{te}$ for generating image $I$
   \ELSE
   \STATE Average embeddings of $L$ to get $C_{naive}$
   \STATE Use $C_{naive}$ for generating image $I$
   \ENDIF
   \STATE Encode $I$ using CLIP to get vector $V_I$
   \STATE Calculate cosine similarity $score$ between $V_I$ and the CLIP embedding of $C_{lc}$
   \STATE Append $score$ to $S_m$
   \ENDFOR
   \ENDFOR
   \ENDFOR
   \STATE \textbf{return} $S_m$ for each model $m$
\end{algorithmic}
\caption{Direct Comparison of Image Generation Models}
\label{alg:direct_comp_img_gen_models}
\end{algorithm}
'''

import logging
logging.getLogger("diffusion.datasets").setLevel(logging.WARNING)

# Import required libraries
import random
import torch
from torchvision import transforms
from torchvision.models import inception_v3, Inception_V3_Weights
from torchvision.transforms.functional import to_tensor
import torch.nn.functional as F
from sentence_transformers import SentenceTransformer
from diffusion.datasets import cifar10_labels, cifar100_labels, get_cifar10, get_cifar100
from diffusion.sampler import Sampler
from scipy.linalg import sqrtm
from transformers import CLIPModel, CLIPProcessor
from trained_models import get_cifar_10_naive, get_cifar_10_text, get_cifar_10_clip, get_cifar_100_naive, get_cifar_100_text, get_cifar_100_clip
from datetime import datetime
from torch.utils.data import DataLoader, Subset
import numpy as np
import json
import itertools

class CLIPScoreCalculator:
    def __init__(self, device):
        self.model_name = "openai/clip-vit-base-patch32"
        self.device = device
        self.clip_model_standlone = CLIPModel.from_pretrained(self.model_name).to(device)
        self.processor = CLIPProcessor.from_pretrained(self.model_name)
        self.naive_sampler_cifar10 = get_cifar_10_naive(device)
        self.text_sampler_cifar10 = get_cifar_10_text(device)
        self.clip_sampler_cifar10 = get_cifar_10_clip(device)
        self.naive_sampler_cifar100 = get_cifar_100_naive(device)
        self.text_sampler_cifar100  = get_cifar_100_text(device)
        self.clip_sampler_cifar100 = get_cifar_100_clip(device)
    
    def preprocess_images_for_clip(self, images):
        """
        Preprocess a batch of images for CLIP.
        """
        if images.dim() == 3:
            images = images.unsqueeze(0)  # Add a batch dimension if needed
        
        # Ensure the tensor is in float format (required for proper normalization)
        images = images.float()

        # Resize the images to fit CLIP's expected input size of 224x224
        images_resized = F.interpolate(images, size=(224, 224), mode='bilinear', align_corners=False)
        
        # Normalize the images using CLIP's expected mean and std
        normalize = transforms.Normalize(mean=[0.48145466, 0.4578275, 0.40821073], std=[0.26862954, 0.26130258, 0.27577711])
        images_normalized = normalize(images_resized)
        
        return images_normalized

    def labels_to_sentence(self, labels):
        '''
        Given a list of labels make a sentence: This is a picture of label1, label2, label3, ... combined
        '''
        return f"This is a picture of {' '.join(labels)} combined."


    def clip_similarity_score(self, image_vector, label_context):
        '''
        Given an image and a label context, calculate the cosine similarity between the image and the label context
        '''
        image_embedding = self.clip_image_to_vector(image_vector)
        label_context_embedding = self.clip_context_to_vector(label_context)
        return torch.nn.functional.cosine_similarity(image_embedding, label_context_embedding)

    def clip_image_to_vector(self, image_tensor):
        """
        Convert an image tensor to a vector using the CLIP model.
        Assumes image_tensor has already been processed with appropriate transforms.

        :param image_tensor: A preprocessed image tensor.
        :param clip_model: The CLIP model.
        :return: A tensor representing the image features (vector) extracted by the CLIP model.
        """
        # Ensure the tensor is on the correct device
        image_tensor = image_tensor.to(self.device)

        # Directly process the image tensor with the model to get features
        with torch.no_grad():  # Assuming you're just doing inference
            image_features = self.clip_model_standlone.get_image_features(pixel_values=image_tensor)

        return image_features

    def clip_context_to_vector(self, label_context):
        """
        Convert a label context to a vector using the CLIP model.
        Assumes label_context has already been processed with appropriate transforms.

        :param label_context: A preprocessed label context.
        :param clip_model: The CLIP model.
        :return: A tensor representing the label context features (vector) extracted by the CLIP model.
        """
        # Tokenize and preprocess the label context
        inputs = self.processor(text=label_context, return_tensors="pt", padding=True, truncation=True).to(self.device)

        # Directly process the label context tensor with the model to get features
        with torch.no_grad():  # Assuming you're just doing inference
            outputs = self.clip_model_standlone.get_text_features(**inputs)

        return outputs
    
    def naive_embedding_combiner(self, labels, naive_sampler):
        '''
        Given a list of labels, combine the embeddings of each label into a single embedding
        '''
        label_embeddings = [naive_sampler.text_to_vector(label) for label in labels]
        return torch.stack(label_embeddings).mean(dim=0)

    def get_single_clip_score(self, labels, n_classes, sampler: Sampler):
        random_n_labels = random.sample(labels, n_classes)
        label_context = self.labels_to_sentence(random_n_labels)
        if (sampler == self.naive_sampler_cifar10 or sampler == self.naive_sampler_cifar100):
            label_context_embedding = self.naive_embedding_combiner(random_n_labels, sampler)
            image_tensor = sampler.sample_vector(label_context_embedding)
        else:
            image_tensor = sampler.sample_list_of_text([label_context])
        prepared_image = self.preprocess_images_for_clip(image_tensor)
        score = self.clip_similarity_score(prepared_image, label_context)
        return score

    def run_clip_experiment(self, n_classes, n_iterations):
        '''
        Direct comparison of image generation models.
        '''
        start = datetime.now()

        datasets = {
            'cifar10': {
                'samplers': {
                    'naive_sampler_cifar10': self.naive_sampler_cifar10, 
                    'text_sampler_cifar10': self.text_sampler_cifar10, 
                    'clip_sampler_cifar10': self.clip_sampler_cifar10
                    },
                'labels': cifar10_labels
            },
            'cifar100': {
                'samplers': {
                    'naive_sampler_cifar100': self.naive_sampler_cifar100, 
                    'text_sampler_cifar100': self.text_sampler_cifar100, 
                    'clip_sampler_cifar100': self.clip_sampler_cifar100
                    },
                'labels': cifar100_labels
            }
        }

        model_scores = {}
        for dataset in datasets:
            model_scores[dataset] = {}
            for model in datasets[dataset]['samplers']:
                model_scores[dataset][model] = []

        for dataset in datasets:
            for i in range(n_iterations):
                for model in datasets[dataset]['samplers']:
                    score = self.get_single_clip_score(datasets[dataset]['labels'], n_classes, datasets[dataset]['samplers'][model])
                    model_scores[dataset][model].append(score.item())
                
                    # Print progress
                    td = (datetime.now() - start).total_seconds()
                    time_per_iteration_estimate = td / (i+1)
                    total_time_estimate = time_per_iteration_estimate * n_iterations
                    time_remaining_estimate = total_time_estimate - td
                    print(f"Iteration: {i+1}/{n_iterations}, Time elapsed: {td:.2f} seconds, Time per iteration: {time_per_iteration_estimate:.2f} seconds, Time remaining: {time_remaining_estimate:.2f} seconds, Total time estimate: {total_time_estimate:.2f} seconds", end='\r')
        
        # Write dictionary to a JSON file
        with open(f'image_generation_models_{n_classes}_labels.json', 'w') as file:
            json.dump(model_scores, file)
                    
        return model_scores

class FIDCalculator:
    def __init__(self, device):
        self.device = device
        self.naive_sampler_cifar10 = get_cifar_10_naive(device)
        self.text_sampler_cifar10 = get_cifar_10_text(device)
        self.clip_sampler_cifar10 = get_cifar_10_clip(device)
        self.naive_sampler_cifar100 = get_cifar_100_naive(device)
        self.text_sampler_cifar100  = get_cifar_100_text(device)
        self.clip_sampler_cifar100 = get_cifar_100_clip(device)
        self.inception_model = inception_v3(weights=Inception_V3_Weights.IMAGENET1K_V1, transform_input=False)
        self.inception_model.fc = torch.nn.Identity()
        self.inception_model.eval()
        self.inception_model.to(device)

    def preprocess_images_for_inception(self, images):
        """
        Preprocess a batch of images for InceptionV3.
        - images: A tensor of shape (N, C, H, W) with the images.
        """
        # Ensure the tensor is in float format (required for proper normalization)
        images = images.float()

        # Resize the images to fit InceptionV3's expected input size of 299x299
        images_resized = F.interpolate(images, size=(299, 299), mode='bilinear', align_corners=False)
        
        # Normalize the images using InceptionV3's expected mean and std
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        images_normalized = normalize(images_resized)
        
        return images_normalized

    def extract_features(self, images, model):
        """
        Extract features from images using the provided model.
        """
        with torch.no_grad():
            features = model(images)
        return features

    def calculate_fid(self, images1, images2, model): # (n, 3, 32, 32), (n, 3, 32, 32
        """
        Calculate the FID score between two sets of images.
        """
        # Preprocess images

        images2 = images2.float() / 255
        images2 = images2 * 2 - 1

        images1_preprocessed = self.preprocess_images_for_inception(images1)
        images2_preprocessed = self.preprocess_images_for_inception(images2)
        
        # Extract features
        features1 = self.extract_features(images1_preprocessed, model)
        features2 = self.extract_features(images2_preprocessed, model)
        
        # Convert features to numpy for FID calculation
        features1_np = features1.detach().cpu().numpy()
        features2_np = features2.detach().cpu().numpy()
        
        # Calculate mean and covariance for both sets
        mu1, sigma1 = np.mean(features1_np, axis=0), np.cov(features1_np, rowvar=False)
        mu2, sigma2 = np.mean(features2_np, axis=0), np.cov(features2_np, rowvar=False)

        eps = 1e-6  # Small regularization term
        sigma1 += np.eye(sigma1.shape[0]) * eps
        sigma2 += np.eye(sigma2.shape[0]) * eps
        
        # Calculate the squared difference in means
        ssdiff = np.sum((mu1 - mu2) ** 2.0)
        
        # Calculate sqrt of product between cov
        covmean = sqrtm(sigma1.dot(sigma2))
        if np.iscomplexobj(covmean):
            covmean = covmean.real
        
        # Calculate FID
        fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
        return fid

    def get_random_label_dataset(self, dataset, label, num_samples=10):
        targets = torch.tensor(dataset.targets)
        label_indices = torch.where(targets == label)[0]
        
        # Ensure num_samples does not exceed the number of available samples for the label
        num_samples = min(num_samples, len(label_indices))
        
        # Randomly select indices
        if num_samples < len(label_indices):
            selected_indices = np.random.choice(label_indices, num_samples, replace=False)
        else:
            selected_indices = label_indices
        
        subset = Subset(dataset, selected_indices)
        
        # Use DataLoader for efficient loading
        loader = DataLoader(subset, batch_size=len(subset), shuffle=False)
        
        # Get a single batch from the DataLoader
        for images, _ in loader:
            return images  # This will be a single tensor containing all images

    def get_single_fid_score(self, label, labels, n=10, sampler: Sampler = None, dataset = None):
        ground_truth_tensor = self.get_random_label_dataset(dataset, labels.index(label), n).to(self.device)
        image_tensors_sampler = sampler.sample_list_of_text([label]*n).to(self.device)
        fid_score = self.calculate_fid(ground_truth_tensor, image_tensors_sampler, self.inception_model)

        return fid_score

    def run_fid_test_experiment_cifar10(self, label, n_batch):
        dataset = get_cifar10()
        fid_score_naive_cifar10 = self.get_single_fid_score(label, cifar10_labels, n_batch, self.naive_sampler_cifar10, dataset)
        print(f"FID Score Naive CIFAR-10 with label {label}: {fid_score_naive_cifar10}")

        fid_score_text_embedding_cifar10 = self.get_single_fid_score(label, cifar10_labels, n_batch, self.text_sampler_cifar10, dataset)
        print(f"FID Score Text Embedding CIFAR-10 with label {label}: {fid_score_text_embedding_cifar10}")

        fid_score_clip_cifar10 = self.get_single_fid_score(label, cifar10_labels, n_batch, self.clip_sampler_cifar10, dataset)
        print(f"FID Score CLIP CIFAR-10 with label {label}: {fid_score_clip_cifar10}")
    
    def run_fid_test_experiment_cifar100(self, label, n_batch):
        dataset = get_cifar100()
        fid_score_naive_cifar100 = self.get_single_fid_score(label, cifar100_labels, n_batch, self.naive_sampler_cifar100, dataset)
        print(f"FID Score Naive CIFAR-100 with label {label}: {fid_score_naive_cifar100}")

        fid_score_text_embedding_cifar100 = self.get_single_fid_score(label, cifar100_labels, n_batch, self.text_sampler_cifar100, dataset)
        print(f"FID Score Text Embedding CIFAR-100 with label {label}: {fid_score_text_embedding_cifar100}")

        fid_score_clip_cifar100 = self.get_single_fid_score(label, cifar100_labels, n_batch, self.clip_sampler_cifar100, dataset)
        print(f"FID Score CLIP CIFAR-100 with label {label}: {fid_score_clip_cifar100}")
    
    def run_fid_experiment(self, n_batch, n_classes):
        """
        Run the FID experiment for a given number of batches and classes.

        :param n_batch: Number of batches to sample for each diffusion run.
        :param n_classes: Number of classes to sample for each diffusion run through random selection.
        """
        start = datetime.now()

        datasets = {
            'cifar10': {
                'samplers': {
                    'naive_sampler_cifar10': self.naive_sampler_cifar10, 
                    'text_sampler_cifar10': self.text_sampler_cifar10, 
                    'clip_sampler_cifar10': self.clip_sampler_cifar10
                    },
                'labels': cifar10_labels,
                'dataset': get_cifar10()
            },
            'cifar100': {
                'samplers': {
                    'naive_sampler_cifar100': self.naive_sampler_cifar100, 
                    'text_sampler_cifar100': self.text_sampler_cifar100, 
                    'clip_sampler_cifar100': self.clip_sampler_cifar100
                    },
                'labels': cifar100_labels,
                'dataset': get_cifar100()
            }
        }

        scores = {}
        for dataset in datasets:
            scores[dataset] = {}
            for model in datasets[dataset]['samplers']:
                scores[dataset][model] = []

        for dataset in datasets:
            for i in range(n_classes):
                label = random.choice(datasets[dataset]['labels'])
                for model in datasets[dataset]['samplers']:
                    fid_score = self.get_single_fid_score(label, datasets[dataset]['labels'], n_batch, datasets[dataset]['samplers'][model], datasets[dataset]['dataset'])
                    scores[dataset][model].append(fid_score)
                    
                    # Print progress
                    td = (datetime.now() - start).total_seconds()
                    time_per_iteration_estimate = td / (i+1)
                    total_time_estimate = time_per_iteration_estimate * n_classes
                    time_remaining_estimate = total_time_estimate - td
                    print(f"Iteration: {i+1}/{n_classes}, Sampler: {model}, Time elapsed: {td:.2f} seconds, Time per iteration: {time_per_iteration_estimate:.2f} seconds, Time remaining: {time_remaining_estimate:.2f} seconds, Total time estimate: {total_time_estimate:.2f} seconds", end='\r')
        
        # save to json
        with open(f'fid_scores_{n_batch}_{n_classes}.json', 'w') as file:
            json.dump(scores, file)

class ISCalculator:
    def __init__(self, device):
        self.device = device
        self.naive_sampler_cifar10 = get_cifar_10_naive(device)
        self.text_sampler_cifar10 = get_cifar_10_text(device)
        self.clip_sampler_cifar10 = get_cifar_10_clip(device)
        self.naive_sampler_cifar100 = get_cifar_100_naive(device)
        self.text_sampler_cifar100  = get_cifar_100_text(device)
        self.clip_sampler_cifar100 = get_cifar_100_clip(device)
        self.inception_model = inception_v3(weights=Inception_V3_Weights.IMAGENET1K_V1, transform_input=False)
        self.inception_model.fc = torch.nn.Identity()
        self.inception_model.eval()
        self.inception_model.to(device)

    def naive_embedding_combiner(self, labels, naive_sampler):
        '''
        Given a list of labels, combine the embeddings of each label into a single embedding
        '''
        label_embeddings = [naive_sampler.text_to_vector(label) for label in labels]
        return torch.stack(label_embeddings).mean(dim=0)

    def labels_to_sentence(self, labels):
        '''
        Given a list of labels make a sentence: This is a picture of label1, label2, label3, ... combined
        '''
        return f"This is a picture of {' '.join(labels)} combined."

    def preprocess_images_for_inception(self, images):
        """
        Preprocess a batch of images for InceptionV3.
        """
        if images.dim() == 3:
            images = images.unsqueeze(0)  # Now the shape will be [1, 3, 32, 32]
        images = images.float()  # Ensure float
        images_resized = F.interpolate(images, size=(299, 299), mode='bilinear', align_corners=False)
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        images_normalized = normalize(images_resized)
        return images_normalized

    def get_model_predictions(self, images):
        """
        Get the softmax predictions from the inception model.
        """
        images_preprocessed = self.preprocess_images_for_inception(images.to(self.device))
        with torch.no_grad():
            preds = torch.nn.functional.softmax(self.inception_model(images_preprocessed), dim=1)
        return preds

    def calculate_is(self, images): # (n, 3, 32, 32)
        """
        Calculate the Inception Score for a batch of images.
        """
        # Preprocess images
        preds = self.get_model_predictions(images)
        preds = preds.cpu().numpy()
        eps = 1e-16
        kl_div = preds * (np.log(preds + eps) - np.log(np.expand_dims(np.mean(preds + eps, 0), 0)))
        kl_div = np.mean(np.sum(kl_div, 1))
        is_score = np.exp(kl_div)
        return is_score
    
    def get_single_is_score(self, n, sampler: Sampler, dataset, n_labels):
        if n_labels == 1:
            labels = random.choices(dataset, k=n)
            image_tensors = sampler.sample_list_of_text(labels)
        else:
            grouped_labels = [random.choices(dataset, k=n_labels) for _ in range(n)]
            if sampler == self.naive_sampler_cifar10 or sampler == self.naive_sampler_cifar100:
                combined_embeddings = [self.naive_embedding_combiner(labels, sampler) for labels in grouped_labels]
                combined_images = [sampler.sample_vector(embedding) for embedding in combined_embeddings]
                image_tensors = torch.stack(combined_images)
            else:
                image_tensors = sampler.sample_list_of_text([self.labels_to_sentence(labels) for labels in grouped_labels])
        is_score = self.calculate_is(image_tensors)
        return is_score

    def run_is_test_experiment_cifar10(self, n_batch, n_labels=1):
        is_score_naive_cifar10 = self.get_single_is_score(n_batch, self.naive_sampler_cifar10, cifar10_labels, n_labels)
        print(f"IS Score Naive CIFAR-10 with {n_labels} labels combined: {is_score_naive_cifar10}")

        is_score_text_embedding_cifar10 = self.get_single_is_score(n_batch, self.text_sampler_cifar10, cifar10_labels, n_labels)
        print(f"IS Score Text Embedding CIFAR-10 with {n_labels} labels combined: {is_score_text_embedding_cifar10}")

        is_score_clip_cifar10 = self.get_single_is_score(n_batch, self.clip_sampler_cifar10, cifar10_labels, n_labels)
        print(f"IS Score CLIP CIFAR-10 with {n_labels} labels combined: {is_score_clip_cifar10}")
import torch
from diffusion.training import DiffusionTrainer
from diffusion.models import UNetModular
from diffusion.datasets import get_cifar100

# Build model
model = UNetModular(
    context_emb_dims=10,
    c_in=3,
    c_out=3,
    time_dim=256
).to("cuda")

# Construct trainer
trainer = DiffusionTrainer(
    noise_steps=1000,
    beta_start=1e-4,
    beta_end=0.02,
    device="cuda",
    epochs=700,
    seed=42,
    batch_size=220,
    fp16=True,
    num_workers=1,
    lr=5e-3,
    use_ema=True,
    unconditional=True,
    plot_interval=20,
)

# Train the model
model, ema = trainer.train(model, get_cifar100())

# Save the models
torch.save(model.state_dict(), "./models/cifar100_base.pth")
torch.save(ema.state_dict(), "./models/cifar100_base_ema.pth")
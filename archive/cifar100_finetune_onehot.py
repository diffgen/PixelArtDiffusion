import torch
from diffusion.training import DiffusionTrainer
from diffusion.models import UNetModular
from diffusion.datasets import get_cifar100

# Construct model
model = UNetModular(
    context_emb_dims=10,
    c_in=3,
    c_out=3,
    time_dim=256
).to("cuda")

# Restore weights
model.load_state_dict(torch.load("./models/cifar100_base_ema.pth"))

# Update model to use 100 dim context
model.context_emb_dims = 100
model.to("cuda")

# Setup trainer
trainer = DiffusionTrainer(
    noise_steps=1000,
    beta_start=1e-4,
    beta_end=0.02,
    device="cuda",
    epochs=50,
    seed=42,
    batch_size=220,
    fp16=True,
    num_workers=1,
    use_ema=True,
    unconditional=True,
    plot_interval=1,
    # Use more stable, low learning rate for fine-tuning
    onecycle=True,
    lr=1e-3,
)

# Load data
cifar = get_cifar100()
model, ema = trainer.train(model, cifar)

# Save model
torch.save(model.state_dict(), "./models/cifar100_finetune_onehot.pth")
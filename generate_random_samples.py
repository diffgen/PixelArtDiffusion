from evaluation_lib import *
import pickle

sampler = AllSampler(dataset_name="cifar100")

file_num = 0
for _ in range(2):
    for i in range(2):
        data = sampler.sample_mix_of_vectors(500, i+2)
        pickle.dump(data, open(f"samples/cifar100_random_samples_{file_num}.pkl", "wb"))
        file_num += 1
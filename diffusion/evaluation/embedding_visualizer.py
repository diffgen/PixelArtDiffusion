import umap
import torch
import matplotlib.pyplot as plt

def visualize_embeddings(sampler, labels):

    with torch.no_grad():
        vectors = [sampler.text_to_vector(label).cpu().numpy() for label in labels]

    embedding_2d = umap.UMAP(n_neighbors=10,
                            min_dist=0.3,
                            metric='correlation').fit_transform(vectors)
    
    embedding_3d = umap.UMAP(n_neighbors=10,
                            min_dist=0.3,
                            metric='correlation',
                            n_components=3).fit_transform(vectors)
    
    # Normalize each axis of embedding_3d to be between 0 and 1
    embedding_3d = (embedding_3d - embedding_3d.min(axis=0)) / (embedding_3d.max(axis=0) - embedding_3d.min(axis=0))

    # Plot in 2d, color using 3d (x, y, z) -> (r, g, b)
    plt.scatter(embedding_2d[:, 0], embedding_2d[:, 1], c=embedding_3d)
    for i, txt in enumerate(labels):
        plt.annotate(txt, (embedding_2d[i, 0], embedding_2d[i, 1]))
    plt.show()
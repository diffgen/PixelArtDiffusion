

from dataclasses import dataclass
from typing import List
from fastprogress import progress_bar
import numpy as np
import torch
from tqdm import tqdm

from diffusion.training import DiffusionTrainer
from torch.utils.data import Dataset

@dataclass
class Sampler:
    model: torch.nn.Module
    noise_steps: int = 1000
    beta_start: float = 1e-4
    beta_end: float = 0.02
    seed: int = 42
    channels: int = 3
    image_size: int = 32
    device: str = "cuda"
    label_list: list = None
    text_embedder: any = None

    def __post_init__(self):
        self.trainer = DiffusionTrainer(
            noise_steps=self.noise_steps,
            beta_start=self.beta_start,
            beta_end=self.beta_end,
            seed=self.seed,
            device=self.device
        )
        self.model = self.model.to(self.device)
        # We need to first check if the model has an embedding layer
        if not hasattr(self.model, "emb_layer"):
            raise ValueError("Model does not have an embedding layer")
        # We need to determine the model embedding input type (context vector, or integer label)
        if isinstance(self.model.emb_layer, torch.nn.Embedding):
            self.input_type = "integer_label"
            self.emb_size = self.model.emb_layer.embedding_dim
            if self.label_list is None:
                raise ValueError("label_list is not provided")
            self.original_emb_layer = self.model.emb_layer
            # Swap out embedding layer with identity layer
            self.model.emb_layer = torch.nn.Identity()
        elif isinstance(self.model.emb_layer, torch.nn.Linear):
            self.input_type = "context_vector"
            self.emb_size = self.model.emb_layer.in_features
            if self.text_embedder is None:
                raise ValueError("text_embedder is not provided")
        else:
            raise ValueError("Model embedding layer is not supported")
        
    def text_to_vector(self, text: str):
        if self.input_type == "context_vector":
            emb = self.text_embedder(text)
            # Convert the embedding to a tensor
            emb = torch.tensor(emb, dtype=torch.float32, device=self.device)
            return emb
        elif self.input_type == "integer_label":
            # We need to lookup the text's index in the label_list, which should have a matching entry
            text = text.lower()
            # Try lookup the index of the string in the label_list
            try:
                index = self.label_list.index(text)
            except ValueError:
                raise ValueError(f"Text {text} is not in the label_list")
            # Now, we convert the index to the embedding vector using the model's embedding layer
            emb = self.original_emb_layer(torch.tensor(index, dtype=torch.long, device=self.device))
            return emb
        else:
            raise ValueError("Model embedding layer is not supported")
        
    def sample_batched_vector(self, batched_vector: torch.Tensor, cfg=3, progress=False):
        n_samples = batched_vector.shape[0]
        images = self.trainer.sample((n_samples, self.channels, self.image_size, self.image_size), self.model, batched_vector, cfg, progress=progress)
        # images = self.trainer.sample_ddim((n_samples, self.channels, self.image_size, self.image_size), self.model, batched_vector, cfg)
        return images

    def sample_vector(self, vector, cfg=3, progress=False):
        # Check if there is batch dimension, if not, add it
        if len(vector.shape) == 1:
            vector = vector.unsqueeze(0)
        # Sample the image
        images = self.sample_batched_vector(vector, cfg=cfg, progress=progress)
        return images[0]

    def sample_list_of_text(self, text_list, cfg=3, progress=False):
        # Convert the text to vectors
        vectors = [self.text_to_vector(text) for text in text_list]
        # Sample the images
        images = self.sample_batched_vector(torch.stack(vectors), cfg=cfg, progress=progress)
        return images
    
    def sample_text(self, text, cfg=3, progress=False):
        return self.sample_list_of_text([text], cfg=cfg, progress=progress)[0]
    
@dataclass
class DatasetSampler:
    dataset: Dataset
    label_list: list = None

    def __post_init__(self):
        # Index the labels of the dataset
        self.indexed_dataset = {}
        for i, (image, label) in enumerate(tqdm(self.dataset)):
            if label not in self.indexed_dataset:
                self.indexed_dataset[label] = []
            self.indexed_dataset[label].append(i)
        # Convert to numpy arrays
        for label in self.indexed_dataset:
            self.indexed_dataset[label] = np.array(self.indexed_dataset[label])
        
    def text_to_vector(self, text: str):
        # Lookup the index of the string in the label_list
        text = text.lower()
        # Replace spaces with underscores
        text = text.replace(" ", "_")
        try:
            index = self.label_list.index(text)
            return index
        except ValueError:
            raise ValueError(f"Text {text} is not in the label_list")

    def sample_vector(self, vector: int, progress=False):
        # Sample from the appropriate label
        indices = self.indexed_dataset[vector]
        # Sample an index
        index = np.random.choice(indices)
        # Get the image
        image, label = self.dataset[index]
        return image

    def sample_batched_vector(self, batched_vector: List[int], progress=False):
        if progress:
            pbar = progress_bar(batched_vector, leave=False)
        else:
            pbar = batched_vector
        images = []
        for vector in pbar:
            images.append(self.sample_vector(vector))
        return torch.stack(images)

    def sample_list_of_text(self, text_list, progress=False):
        # Convert the text to vectors
        vectors = [self.text_to_vector(text) for text in text_list]
        # Sample the images
        images = self.sample_batched_vector(vectors, progress=progress)
        return images
    
    def sample_text(self, text, progress=False):
        return self.sample_list_of_text([text], progress=progress)[0]
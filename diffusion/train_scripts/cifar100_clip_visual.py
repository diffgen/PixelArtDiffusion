from fastprogress import progress_bar
import torch
from diffusion.preprocessing import standardize_image_rgb
from diffusion.training import DiffusionTrainer
from diffusion.models import UNetModular
import torch.nn as nn
from diffusion.utils import current_file_name
from torchvision import datasets
import os
from diffusion.datasets import cifar100_labels

# Configure environment
os.environ["TOKENIZERS_PARALLELISM"] = "false"
from sentence_transformers import SentenceTransformer

def get_models(device):

    # Get embedding model
    embedding_model = SentenceTransformer('clip-ViT-B-32', device=device)

    # Get embeddings
    embeddings = embedding_model.encode(cifar100_labels)
    embedding_length = len(embeddings[0])

    # Build model
    model = UNetModular(
        emb_layer=nn.Linear(embedding_length, 256),
        c_in=3,
        c_out=3,
        time_dim=256
    ).to(device)

    return model, embedding_model, embeddings
    
class CIFAR100CLIPDataset:
    def __init__(self, transform=None):
        self.raw_cifar100 = datasets.CIFAR100(root="datasets/cifar100", download=True)
        clip_model = SentenceTransformer('clip-ViT-B-32')
        # Image encodings:
        self.image_encodings = []
        for image, _ in progress_bar(self.raw_cifar100):
            self.image_encodings.append(clip_model.encode(image))
        del clip_model
        self.transform = transform

    def __len__(self):
        # return len(self._cache)
        return len(self.image_encodings)
    
    def __getitem__(self, idx):
        image, _ = self.raw_cifar100[idx]
        if self.transform:
            image = self.transform(image)
        return image, self.image_encodings[idx]

def get_dataset():
    return CIFAR100CLIPDataset(transform=standardize_image_rgb(32))

if __name__ == "__main__":

    device = "cuda"

    model, embedding_model, embeddings = get_models(device)
    dataset = get_dataset()

    embedding_model.to("cpu") # Free up VRAM

    # Construct trainer
    trainer = DiffusionTrainer(
        wandb_project_name="diffusion-generalization",
        wandb_run_name=current_file_name(),
        # Data parameters
        unconditional=False, # Allow labels to be fed in
        # Training parameters
        lr=8e-4,
        epochs=350,
        batch_size=80,
        device=device,
        onecycle=False,
    )

    # Train the model
    model, ema = trainer.train(model, dataset)


    # Make sure ./final_models exists
    os.makedirs("./final_models", exist_ok=True)

    # Save the models according to the name of this file
    torch.save(model.state_dict(), f"./final_models/{current_file_name()}.pth")
    torch.save(ema.state_dict(), f"./final_models/{current_file_name()}_ema.pth")
import torch
from diffusion.training import DiffusionTrainer
from diffusion.models import UNetModular
from diffusion.datasets import get_cifar10
import torch.nn as nn
from diffusion.utils import current_file_name
import os

def get_models(device):

    # Build model
    return UNetModular(
        emb_layer=nn.Embedding(10, 256), # Basic embedding layer, no context
        c_in=3,
        c_out=3,
        time_dim=256
    ).to(device)

def get_dataset():
    return get_cifar10()

if __name__ == "__main__":

    device = "cuda"

    # Construct trainer
    trainer = DiffusionTrainer(
        wandb_project_name="diffusion-generalization",
        wandb_run_name=current_file_name(),
        # Data parameters
        unconditional=False, # Allow labels to be fed in
        # Training parameters
        lr=8e-4,
        epochs=350,
        batch_size=800,
        device="cuda",
        onecycle=False
    )

    # Get model
    model = get_models(device)

    # Get dataset
    dataset = get_dataset()

    # Train the model
    model, ema = trainer.train(model, dataset)

    # Make sure ./final_models exists
    os.makedirs("./final_models", exist_ok=True)

    # Save the models according to the name of this file
    torch.save(model.state_dict(), f"./final_models/{current_file_name()}.pth")
    torch.save(ema.state_dict(), f"./final_models/{current_file_name()}_ema.pth")
from typing import List
from sentence_transformers import SentenceTransformer
import numpy as np

# Encoder wrapper for SentenceTransformer

class SentenceTransformerEmbedding:
    def __init__(self, model="all-MiniLM-L6-v2"):
        # Check out models here: https://www.sbert.net/docs/pretrained_models.html
        self.model = SentenceTransformer(model)
    
    def __call__(self, labels: List[str]) -> np.ndarray:
        return self.model.encode(labels)
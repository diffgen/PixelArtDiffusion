from typing import List
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

def pca_visualize_embeddings(labels: List[str], embeddings: np.ndarray):
    # Plot the embeddings using 3D PCA
    pca = PCA(n_components=3)
    pca.fit(embeddings)
    embeddings_pca = pca.transform(embeddings)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(embeddings_pca[:, 0], embeddings_pca[:, 1], embeddings_pca[:, 2])
    for i, label in enumerate(labels):
        ax.text(embeddings_pca[i, 0], embeddings_pca[i, 1], embeddings_pca[i, 2], label)
    plt.show()
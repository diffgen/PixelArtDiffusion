from typing import List
import numpy as np

class BagOfWordsEmbedding:

    def __init__(self, labels: List[str], ignore_out_of_vocab: bool = True):
        self.ignore_out_of_vocab = ignore_out_of_vocab
        labels = [x.lower() for x in list(set(labels))]
        # If labels are compound (e.g. "red car"), split them into individual words
        self.labels = [x for label in labels for x in label.split()]
        self.vocab_set = set(self.labels)
        self.vocab_ordered_list = list(sorted(self.vocab_set))
        self.len = len(self.vocab_ordered_list)

    def get_individual_word_embedding(self, word: str) -> List[int]:
        # Assumes already lowercased
        if word not in self.vocab_set:
            if not self.ignore_out_of_vocab:
                raise ValueError(f"Word {word} not in vocabulary")
            else:
                return np.zeros(self.len)
        else:
            vec = np.zeros(self.len)
            vec[self.vocab_ordered_list.index(word)] = 1
            return vec
        
    def get_compound_word_embedding(self, text: str) -> List[int]:
        # If text is not string, complain
        if not isinstance(text, str):
            raise ValueError(f"Expected string, got {type(text)}")
        # Average over the words
        text = text.lower()
        text = text.split()
        return np.mean([self.get_individual_word_embedding(word) for word in text], axis=0)

    def __call__(self, labels: List[str]) -> List[List[int]]:
        # Raise error if its not a list
        if not isinstance(labels, list):
            raise ValueError(f"Expected list, got {type(labels)}")
        return np.array([self.get_compound_word_embedding(label) for label in labels], dtype=np.float32)
from matplotlib import pyplot as plt
from torchvision import transforms

class ImageProcessor:
    def __init__(self, height: int = 32):
        self.height = height
        self.image_transform = transforms.Compose([
            transforms.Resize(height),
            transforms.CenterCrop(height),
            transforms.ToTensor(),
            # transforms.Normalize((0.5,), (0.5,))
        ])
    def __call__(self, image):
        return self.image_transform(image)
    
def show_dataset(dataset, n=6):
    fig, ax = plt.subplots(1, n, figsize=(20, 5))
    for i in range(n):
        img, _ = dataset[i]
        ax[i].imshow(img.permute(1, 2, 0))
        ax[i].axis("off")
    plt.show()
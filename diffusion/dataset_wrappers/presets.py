from torchvision import datasets, transforms

from diffusion.datasets.CustomDataset import CustomDataset

def load_cifar_dataset(augment_sentences=False):
    transform = transforms.Compose([
        transforms.Resize(16),
        transforms.CenterCrop(16),
        transforms.ToTensor()
    ])
    # Target transform to change index to text
    label_map = {
        0: "airplane",
        1: "automobile",
        2: "bird",
        3: "cat",
        4: "deer",
        5: "dog",
        6: "frog",
        7: "horse",
        8: "ship",
        9: "truck"
    }
    # Further transform each to f"Image of a {label}"
    if augment_sentences:
        label_map = {k: f"Image of a {v}" for k, v in label_map.items()}
    target_transform = lambda x: label_map[x]
    cifar = datasets.CIFAR10(root='./data', train=True, download=True, transform=transform, target_transform=target_transform)
    return cifar

cifar_vocab = [
    "airplane",
    "automobile",
    "bird",
    "cat",
    "deer",
    "dog",
    "frog",
    "horse",
    "ship",
    "truck"
]

def load_pixel_art_dataset():
    image_path = './dataset/sprites_1788_16x16.npy' # Sprite dataset
    label_path = './dataset/sprite_labels_nc_1788_16x16.npy' # Sprite labels
    pixel = CustomDataset(image_path, label_path)
    return pixel
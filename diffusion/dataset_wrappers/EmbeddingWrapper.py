from torch.utils.data import Dataset
import functools

class EmbeddingWrapper(Dataset):
    '''
    Wraps a dataset and an embedding function to return (image, embedding) pairs
    '''

    def __init__(self, dataset, embedding):
        self.dataset = dataset
        self.embedding = embedding

    @functools.lru_cache(maxsize=10000) # Cache the embeddings
    def embed_one(self, idx):
        image, label = self.dataset[idx]
        embedding_list = self.embedding([label])
        return embedding_list[0]
    
    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self, idx):
        image, label = self.dataset[idx]
        embedding = self.embed_one(idx)
        return image, embedding
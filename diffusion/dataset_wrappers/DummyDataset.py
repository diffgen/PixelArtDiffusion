import numpy as np
import torch
from torch.utils.data import Dataset
from torchvision import transforms

class DummyDataset(Dataset):
    def __init__(self, image_size, label_length, num_images):
        self.image_size = (3, image_size, image_size)
        self.label_length = label_length
        self.num_images = num_images

    # Return the number of images in the dataset
    def __len__(self):
        return self.num_images
    
    # Get the image and label at a given index
    def __getitem__(self, idx):
        return (torch.rand(self.image_size), torch.randint(0, 10, (self.label_length,)))

    def getshapes(self):
        # return shapes of images and labels
        return self.image_size, self.label_length
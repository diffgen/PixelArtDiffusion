from torch.utils.data import Dataset
import os
from PIL import Image, ImageFile
from torchvision import transforms

from diffusion.datasets import ImageProcessor

ImageFile.LOAD_TRUNCATED_IMAGES = True

class ImageNette(Dataset):
    def __init__(self, path="./data/imagenette2-320/", split="train", image_transform=ImageProcessor(height=32), label_transform=None):
        self.path = path
        self.split = split
        self.image_transform = image_transform
        self.label_transform = label_transform
        # Read all folders in <path>/<split>
        prelim_data = [] # (full path, label)
        self.folders = [f for f in os.listdir(os.path.join(path, split)) if os.path.isdir(os.path.join(path, split, f))]
        # Now iterate over the folder names. Extract the folder name since it is the label
        for label in self.folders:
            for f in os.listdir(os.path.join(path, split, label)):
                # Add (full path, label) to the list
                prelim_data.append((os.path.join(path, split, label, f), label))
        # Now, we load in the human readable labels from <path>/human_labels.txt
        self.imagenet_human_label_lookup = {}
        with open(os.path.join(path, "human_labels.txt"), "r") as f:
            # Space separated <label> <idx> <human readable label>
            for line in f:
                label, idx, human_label = line.strip().split()
                self.imagenet_human_label_lookup[label] = human_label.replace("_", " ")
        # Now, we need to actually load the images using PIL, and further, read in the text labels
        self.data = []
        self.labels = set()
        for path, label in prelim_data:
            img_file = Image.open(path)
            # Copy to memory as numpy array and close the file
            img = img_file.copy()
            img_file.close()
            # If image is monochrome, convert to RGB
            if img.mode != "RGB":
                img = img.convert("RGB")
            self.data.append((img, self.imagenet_human_label_lookup[label]))
            self.labels.add(self.imagenet_human_label_lookup[label])
        self.labels = list(self.labels)

    def __len__(self):
        return len(self.data)
    
    def __getitem__(self, idx):
        image, label = self.data[idx]
        if self.image_transform:
            image = self.image_transform(image)
        if self.label_transform:
            label = self.label_transform(label)
        return image, label

    def get_all_labels(self):
        return self.labels
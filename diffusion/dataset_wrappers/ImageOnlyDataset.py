import os
from torch.utils.data import Dataset
from torchvision import transforms
from PIL import Image

class ImageOnlyDataset(Dataset):
    def __init__(self, image_folder: str, resolution: int=16):
        self.image_folder = image_folder
        self.image_list = os.listdir(image_folder)
        # Load all .jpg / .jpeg / .png files from the image folder
        self.image_list = [f for f in self.image_list if f.endswith(".jpg") or f.endswith(".jpeg") or f.endswith(".png")]
        self.images = [Image.open(os.path.join(image_folder, f)) for f in self.image_list]
        self.transform = transforms.Compose([
            transforms.Resize(resolution),
            transforms.CenterCrop(resolution),
            transforms.ToTensor()
        ])
        self.images = [self.transform(img) for img in self.images]
    def __len__(self):
        return len(self.images)
    def __getitem__(self, idx):
        return self.images[idx], 0
from torch.utils.data import Dataset

class NoEmbeddingWrapper(Dataset):
    def __init__(self, dataset: Dataset):
        self.dataset = dataset

    def __getitem__(self, index):
        image, label = self.dataset[index]
        return image, 0

    def __len__(self):
        return len(self.dataset)
import torch
from torch.utils.data import Dataset

class InMemoryDataset(Dataset):
    def __init__(self, base_dataset):
        self.data = []
        # Iterate through the base dataset and load all samples into memory
        for item in base_dataset:
            self.data.append(item)

    def __len__(self):
        # Return the total number of samples
        return len(self.data)

    def __getitem__(self, idx):
        # Return a sample from memory
        return self.data[idx]

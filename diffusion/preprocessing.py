import numpy as np
import torch
import torchvision
import torchvision.transforms as T
import torch.nn as nn

def standardize_image_rgb(size: int):
    return T.Compose([
        T.Resize(size),
        T.RandomResizedCrop(size, scale=(0.8, 1.0)),
        T.ToTensor(),
        T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
    ])

def tensorize_image_rgb(size: int): # We want it to still be in [0, 255] int
    return T.Compose([
        T.Resize(size),
        T.RandomResizedCrop(size, scale=(0.8, 1.0)),
        T.ToTensor(),
        # Multiply by 255 and turn into uint8
        lambda x: (x * 255).to(torch.uint8),
    ])

def int_to_one_hot(num_classes):
    return lambda x: nn.functional.one_hot(torch.tensor(x), num_classes).float()

def figr8_image_transform(size):
    return T.Compose([
        T.Resize(size),
        T.CenterCrop(size),
        T.ToTensor(),
        # Gray scale
        T.Grayscale(num_output_channels=1),
    ])

def int_to_one_hot_numpy(num_classes):
    return lambda x: np.eye(num_classes)[x].astype(np.float32)
import logging, copy

import torch
from torch import optim
import torch.nn as nn
import numpy as np
from fastprogress import progress_bar

import wandb
from diffusion.utils import *
from diffusion.models import EMA
from torch.utils.data import DataLoader

import os
from dataclasses import dataclass

def single_yes_or_no_question(question, default_no=True):
    choices = ' [y/N]: ' if default_no else ' [Y/n]: '
    default_answer = 'n' if default_no else 'y'
    reply = str(input(question + choices)).lower().strip() or default_answer
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return False if default_no else True

@dataclass
class DiffusionTrainer:
    wandb: bool = True
    wandb_run_name: str = "diffusion_run"
    wandb_project_name: str = "diffusion"
    noise_steps: int = 1000
    beta_start: float = 1e-4
    beta_end: float = 0.02
    device: str = "cuda"
    epochs: int = 100
    seed: int = 42
    batch_size: int = 10
    slice_size: int = 1
    fp16: bool = True
    num_workers: int = 1
    lr: float = 5e-3
    use_ema: bool = True
    unconditional: bool = False
    plot_interval: int = 5
    save_interval: int = 10
    onecycle: bool = True
    root_save_dir: str = "models"

    def __post_init__(self):
        self.beta = self.prepare_noise_schedule().to(self.device)
        self.alpha = 1. - self.beta
        self.alpha_hat = torch.cumprod(self.alpha, dim=0)

    # Noise schedule

    def prepare_noise_schedule(self):
        return torch.linspace(self.beta_start, self.beta_end, self.noise_steps)
    
    def sample_timesteps(self, n):
        return torch.randint(low=1, high=self.noise_steps, size=(n,))

    def noise_images(self, x, t):
        "Add noise to images at instant t"
        sqrt_alpha_hat = torch.sqrt(self.alpha_hat[t])[:, None, None, None]
        sqrt_one_minus_alpha_hat = torch.sqrt(1 - self.alpha_hat[t])[:, None, None, None]
        Ɛ = torch.randn_like(x)
        return sqrt_alpha_hat * x + sqrt_one_minus_alpha_hat * Ɛ, Ɛ

    def prepare(self, model, num_samples_per_epoch):
        self.optimizer = optim.AdamW(model.parameters(), lr=self.lr, eps=1e-5)
        if self.onecycle:
            self.scheduler = optim.lr_scheduler.OneCycleLR(self.optimizer, max_lr=self.lr, 
                                                 steps_per_epoch=num_samples_per_epoch, epochs=self.epochs)
        self.mse = nn.MSELoss()
        self.ema = EMA(0.995)
        self.scaler = torch.cuda.amp.GradScaler()

    def train_step(self, model, ema_model, loss):
        self.optimizer.zero_grad()
        self.scaler.scale(loss).backward()
        self.scaler.step(self.optimizer)
        self.scaler.update()
        if self.use_ema:
            self.ema.step_ema(ema_model, model)
        if self.onecycle:
            self.scheduler.step()

    def get_epoch_save_dir(self, epoch):
        # <root_save_dir>/<wandb_run_name>/<epoch>
        save_dir = os.path.join(self.root_save_dir, self.wandb_run_name, str(epoch))
        os.makedirs(save_dir, exist_ok=True)
        return save_dir

    def one_epoch(self, i_epoch, model, ema_model, data_loader, train=True, show_n_images=5):
        avg_loss = 0.
        if train: model.train()
        else: model.eval()
        pbar = progress_bar(data_loader, leave=False)
        last_label_batch = None
        last_image_batch = None
        for batch_idx, (images, labels) in enumerate(pbar):

            if self.unconditional:
                labels = None

            last_label_batch = labels
            last_image_batch = images

            with torch.autocast("cuda") and (torch.inference_mode() if not train else torch.enable_grad()):
                images = images.to(self.device)
                if labels is not None:
                    labels = labels.to(self.device)
                t = self.sample_timesteps(images.shape[0]).to(self.device)
                x_t, noise = self.noise_images(images, t)
                if np.random.random() < 0.1:
                    labels = None
                predicted_noise = model(x_t, t, labels)
                loss = self.mse(noise, predicted_noise)
                avg_loss += loss
            if train:
                self.train_step(model, ema_model, loss)
                if self.onecycle and self.wandb:
                    wandb.log({"train_mse": loss.item(),
                                "learning_rate": self.scheduler.get_last_lr()[0]})
                else:
                    if self.wandb:
                        wandb.log({"train_mse": loss.item(), "learning_rate": self.lr})
            pbar.comment = f"MSE={loss.item():2.3f}; Epoch={i_epoch}/{self.epochs}; Batch={batch_idx}/{len(data_loader)}"

        # Image shape
        image_shape = last_image_batch[-show_n_images:].shape
        # Truncated batch to last n
        if self.unconditional:
            last_n_labels = show_n_images
        else:
            last_n_labels = last_label_batch[-show_n_images:].to(self.device)
        
        # print(f"Train: {train} - Epoch: {i_epoch}, {i_epoch % self.plot_interval} - Loss: {avg_loss.mean().item()}")
        if train and (i_epoch % self.plot_interval) == 0:
            self.log_images(model, ema_model, image_shape, last_n_labels)
        if train and (i_epoch % self.save_interval) == 0 or i_epoch == self.epochs - 1: # If it's the last epoch, save the model
            save_dir = self.get_epoch_save_dir(i_epoch)
            self.save_state(model, ema_model, save_dir)
        return avg_loss.mean().item()

    def save_state(self, model, ema_model, save_dir):
        model_path = os.path.join(save_dir, "model.pth")
        torch.save(model.state_dict(), model_path)
        if self.use_ema:
            torch.save(ema_model.state_dict(), os.path.join(save_dir, "ema_model.pth"))
            # Save the optimizer state
        torch.save(self.optimizer.state_dict(), os.path.join(save_dir, "optimizer.pth"))
        if self.onecycle:
            torch.save(self.scheduler.state_dict(), os.path.join(save_dir, "scheduler.pth"))
        # Upload the saved model to wandb as an artifact
        if self.wandb:
            wandb.save(model_path)
            if self.use_ema:
                wandb.save(os.path.join(save_dir, "ema_model.pth"))
    
    def get_config(self):
        # Remove irrelevant fields
        return {k: v for k, v in self.__dict__.items() if k not in ["wandb_run_name", "wandb_project_name", "plot_interval", "save_interval", "root_save_dir", "wandb"]}

    def check_conflicts(self):
        # Check if the save directory already exists
        save_dir = os.path.join(self.root_save_dir, self.wandb_run_name)
        if os.path.exists(save_dir):
            if not single_yes_or_no_question(f"\033[91mSave directory {save_dir} already exists. Overwrite?\033[0m", default_no=True):
                exit(1)
        return False
            
    def train(self, model, dataset, prepare=True):
        self.check_conflicts()
        if self.wandb:
            wandb.init(
                project=self.wandb_project_name, 
                name=self.wandb_run_name,
                config=self.get_config()
            )
        dataloader = self.get_dataloader(dataset, train=True)
        if prepare:
            self.prepare(model, len(dataloader))
        if self.use_ema:
            ema_model = copy.deepcopy(model).eval().requires_grad_(False)
        else:
            ema_model = None
        for epoch in progress_bar(range(self.epochs), total=self.epochs, leave=True):
            logging.info(f"Starting epoch {epoch}:")
            _  = self.one_epoch(epoch, model, ema_model, dataloader, train=True)
        if self.use_ema:
            return model, ema_model
        else:
            return model
        
    def train_custom_loader(self, model, loader):
        self.prepare(model, len(loader))
        if self.use_ema:
            ema_model = copy.deepcopy(model).eval().requires_grad_(False)
        else:
            ema_model = None
        for epoch in progress_bar(range(self.epochs), total=self.epochs, leave=True):
            logging.info(f"Starting epoch {epoch}:")
            _  = self.one_epoch(epoch, model, ema_model, loader, train=True)
        if self.use_ema:
            return model, ema_model
        else:
            return model
    
    @torch.inference_mode()
    def sample(self, noise_shape, model, labels, cfg_scale=3, progress=False):
        if isinstance(labels, int):
            n = labels
            labels = None
        else:
            n = len(labels)

        model.eval()

        x = torch.randn(noise_shape).to(self.device)
        if progress:
            pbar = progress_bar(list(reversed(range(1, self.noise_steps))), leave=False)
        else:
            pbar = reversed(range(1, self.noise_steps))
        for i in pbar:
            t = (torch.ones(n) * i).long().to(self.device) # t can be huge because it gets fed into a sinusoidal embedding
            try:
                predicted_noise = model(x, t, labels)
                if cfg_scale > 0: # if cfg_scale is enabled, we also sample unconditional noise and interpolate
                    uncond_predicted_noise = model(x, t, None) 
                    predicted_noise = torch.lerp(uncond_predicted_noise, predicted_noise, cfg_scale)
            except RuntimeError as e:
                print(f"Shape of x: {x.shape}, t: {t.shape}, labels: {labels.shape if labels is not None else None}")
                raise e
            alpha = self.alpha[t][:, None, None, None]
            alpha_hat = self.alpha_hat[t][:, None, None, None]
            beta = self.beta[t][:, None, None, None]
            if i > 1:
                noise = torch.randn_like(x)
            else:
                noise = torch.zeros_like(x)
            x = 1 / torch.sqrt(alpha) * (x - ((1 - alpha) / (torch.sqrt(1 - alpha_hat))) * predicted_noise) + torch.sqrt(beta) * noise
        x = (x.clamp(-1, 1) + 1) / 2
        x = (x * 255).type(torch.uint8)
        return x
    
    @torch.inference_mode()
    def sample_ddim(self, noise_shape, model, labels, sample_steps=500, cfg_scale=3, eta=0):
        if isinstance(labels, int):
            n = labels
            labels = None
        else:
            n = len(labels)

        model.eval()

        time_step = torch.arange(0, self.noise_steps, (self.noise_steps // sample_steps)).long() + 1 # We start at 1 because we don't want to sample at t=0
        time_step = reversed(torch.cat((torch.tensor([0], dtype=torch.long), time_step))) # We add t=0 to the beginning
        time_step = list(zip(time_step[:-1], time_step[1:])) # We create pairs of (t_i, t_i+1)
        # We remove the first pair
        time_step = time_step[1:]

        x = torch.randn(noise_shape).to(self.device)
        # for i in reversed(range(1, self.noise_steps)):
        for i, p_i in time_step:
            t = (torch.ones(n) * i).long().to(self.device) # t can be huge because it gets fed into a sinusoidal embedding
            p_t = (torch.ones(n) * p_i).long().to(self.device) # t can be huge because it gets fed into a sinusoidal embedding
            try:
                predicted_noise = model(x, t, labels)
                if cfg_scale > 0: # if cfg_scale is enabled, we also sample unconditional noise and interpolate
                    uncond_predicted_noise = model(x, t, None) 
                    predicted_noise = torch.lerp(uncond_predicted_noise, predicted_noise, cfg_scale)
            except RuntimeError as e:
                print(f"Shape of x: {x.shape}, t: {t.shape}, labels: {labels.shape if labels is not None else None}")
                raise e
            # alpha = self.alpha[t][:, None, None, None]
            # alpha_hat = self.alpha_hat[t][:, None, None, None]
            # print(self.alpha_hat.shape)
            # print(t, p_t)
            alpha_t = self.alpha_hat[t][:, None, None, None]
            alpha_prev = self.alpha_hat[p_t][:, None, None, None]
            if i > 1:
                noise = torch.randn_like(x)
            else:
                print("Not adding noise")
                noise = torch.zeros_like(x)
            # x = 1 / torch.sqrt(alpha) * (x - ((1 - alpha) / (torch.sqrt(1 - alpha_hat))) * predicted_noise) + torch.sqrt(beta) * noise
            x0_t = torch.clamp((x - (predicted_noise * torch.sqrt((1 - alpha_t)))) / torch.sqrt(alpha_t), -1, 1)
            c1 = eta * torch.sqrt((1 - alpha_t / alpha_prev) * (1 - alpha_prev) / (1 - alpha_t))
            c2 = torch.sqrt((1 - alpha_prev) - c1 ** 2)
            x = torch.sqrt(alpha_prev) * x0_t + c2 * predicted_noise + c1 * noise
        x = (x.clamp(-1, 1) + 1) / 2
        x = (x * 255).type(torch.uint8)
        return x

    def get_dataloader(self, dataset, train=True):
        if self.slice_size>1:
            dataset = torch.utils.data.Subset(dataset, indices=range(0, len(dataset), self.slice_size))
        if train:
            return DataLoader(dataset, batch_size=self.batch_size, shuffle=True, num_workers=self.num_workers)
        else:
            return DataLoader(dataset, batch_size=2*self.batch_size, shuffle=False, num_workers=self.num_workers)

    def log_images(self, model, ema_model, image_shape, labels):
        if not self.wandb:
            return
        "Log images to wandb and save them to disk"
        sampled_images = self.sample(image_shape, model, labels, cfg_scale=3)
        wandb.log({"sampled_images":     [wandb.Image(img.permute(1,2,0).squeeze().cpu().numpy()) for img in sampled_images]})

        # EMA model sampling
        ema_sampled_images = self.sample(image_shape, ema_model, labels, cfg_scale=3)
        wandb.log({"ema_sampled_images": [wandb.Image(img.permute(1,2,0).squeeze().cpu().numpy()) for img in ema_sampled_images]})
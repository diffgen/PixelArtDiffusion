from evaluation_lib import *
from matplotlib import pyplot as plt
import pickle

sampler = AllSampler(dataset_name="cifar100")

for i in range(2):
    labels = sampler.labels * 5
    in_domain = sampler.sample_list_of_text(labels)
    data = {
        "labels": labels,
        "images": in_domain
    }
    pickle.dump(data, open(f"samples/cifar100_samples_{i}.pkl", "wb"))
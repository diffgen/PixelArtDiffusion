You can download the FIGR8 dataset here:

https://github.com/marcdemers/FIGR-8

Then, recreate the following folder structure:
- datasets/
    - figr8/
        <classes>/
            <image>.png
import logging

from diffusion.preprocessing import tensorize_image_rgb
logging.getLogger("diffusion.datasets").setLevel(logging.WARNING)
from torchvision import datasets

# Import required libraries
import torch
from torchvision import transforms
from torchvision.models import inception_v3, Inception_V3_Weights
import torch.nn.functional as F
from scipy.linalg import sqrtm
import numpy as np
from sentence_transformers import SentenceTransformer

from dataclasses import dataclass
from diffusion.sampler import DatasetSampler
from evaluation_lib import *
from diffusion.datasets import get_cifar10, raw_cifar100_labels, cifar100_labels, get_cifar100, cifar10_labels # Raw has the underscores, the other has it replace with spaces, converted for text embedders to understand
from trained_models import get_cifar_100_naive, get_cifar_100_text, get_cifar_100_clip, get_cifar_10_clip, get_cifar_10_naive, get_cifar_10_text

def convert_images_to_pil(images):
    if isinstance(images, list) or isinstance(images, torch.Tensor):
        if isinstance(images[0], torch.Tensor):
            images = [transforms.ToPILImage()(image) for image in images]
    return images

def convert_images_to_numpy(images):
    if isinstance(images, list):
        if isinstance(images[0], torch.Tensor):
            images = [image.numpy() for image in images]
    return np.array(images)

def convert_images_to_tensor(images):
    if isinstance(images, list):
        if isinstance(images[0], torch.Tensor):
            images = torch.stack(images)
    return images

class CLIPMetrics:
    def __init__(self, device="cuda"):
        self.device = device
        self.clip_model = SentenceTransformer('clip-ViT-B-32', device=device)

    def clip_similarity_score(self, images, texts):
        '''
        Given an image and a label context, calculate the cosine similarity between the image and the label context
        '''
        # If images is a list of torch tensors, convert to list of PIL images
        images = convert_images_to_pil(images)
        image_embedding = self.clip_model.encode(images)
        label_context_embedding = self.clip_model.encode(texts)
        # Calculate cosine similarity 
        similarity_scores = (image_embedding * label_context_embedding).sum(axis=1) / (np.linalg.norm(image_embedding, axis=1) * np.linalg.norm(label_context_embedding, axis=1))
        return similarity_scores


class InceptionMetrics:
    def __init__(self, device="cuda"):
        self.device = device
        self.inception_model = inception_v3(weights=Inception_V3_Weights.IMAGENET1K_V1, transform_input=False)
        self.inception_model.fc = torch.nn.Identity()
        self.inception_model.eval()
        self.inception_model.to(device)

    def preprocess_images_for_inception(self, images):
        """
        Preprocess a batch of images for InceptionV3.
        - images: A tensor of shape (N, C, H, W) with the images.
        """
        # Ensure the tensor is in float format (required for proper normalization)
        images = images.float()

        # Resize the images to fit InceptionV3's expected input size of 299x299
        images_resized = F.interpolate(images, size=(299, 299), mode='bilinear', align_corners=False)
        
        # Normalize the images using InceptionV3's expected mean and std
        normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        images_normalized = normalize(images_resized)
        
        return images_normalized

    def extract_features(self, images, model):
        """
        Extract features from images using the provided model.
        """
        with torch.no_grad():
            features = model(images)
        return features

    def calculate_fid(self, images1, images2, model): # (n, 3, 32, 32), (n, 3, 32, 32
        """
        Calculate the FID score between two sets of images.
        """

        images1 = convert_images_to_tensor(images1).to(self.device)
        images2 = convert_images_to_tensor(images2).to(self.device)

        # Preprocess images

        images2 = images2.float() / 255
        images2 = images2 * 2 - 1

        images1_preprocessed = self.preprocess_images_for_inception(images1)
        images2_preprocessed = self.preprocess_images_for_inception(images2)
        
        # Extract features
        features1 = self.extract_features(images1_preprocessed, model)
        features2 = self.extract_features(images2_preprocessed, model)
        
        # Convert features to numpy for FID calculation
        features1_np = features1.detach().cpu().numpy()
        features2_np = features2.detach().cpu().numpy()
        
        # Calculate mean and covariance for both sets
        mu1, sigma1 = np.mean(features1_np, axis=0), np.cov(features1_np, rowvar=False)
        mu2, sigma2 = np.mean(features2_np, axis=0), np.cov(features2_np, rowvar=False)

        eps = 1e-6  # Small regularization term
        sigma1 += np.eye(sigma1.shape[0]) * eps
        sigma2 += np.eye(sigma2.shape[0]) * eps
        
        # Calculate the squared difference in means
        ssdiff = np.sum((mu1 - mu2) ** 2.0)
        
        # Calculate sqrt of product between cov
        covmean = sqrtm(sigma1.dot(sigma2))
        if np.iscomplexobj(covmean):
            covmean = covmean.real
        
        # Calculate FID
        fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
        return fid
    
    def get_model_is_predictions(self, images):
        """
        Get the softmax predictions from the inception model.
        """
        images_preprocessed = self.preprocess_images_for_inception(images.to(self.device))
        with torch.no_grad():
            preds = torch.nn.functional.softmax(self.inception_model(images_preprocessed), dim=1)
        return preds

    def calculate_is(self, images): # (n, 3, 32, 32)
        """
        Calculate the Inception Score for a batch of images.
        """

        images = convert_images_to_tensor(images).to(self.device)

        # Preprocess images
        preds = self.get_model_is_predictions(images)
        preds = preds.cpu().numpy()
        eps = 1e-16
        kl_div = preds * (np.log(preds + eps) - np.log(np.expand_dims(np.mean(preds + eps, 0), 0)))
        kl_div = np.mean(np.sum(kl_div, 1))
        is_score = np.exp(kl_div)
        return is_score

@dataclass
class AllSampler:
    device: str = "cuda"
    # cifar10 or cifar100 (literals) for typing:
    dataset_name: str = "cifar100"

    def __post_init__(self):

        if self.dataset_name == "cifar100":
            self.dataset = datasets.CIFAR100(root="datasets/cifar100", download=True, transform=tensorize_image_rgb(32))
            self.labels = cifar100_labels
            self.raw_labels = raw_cifar100_labels
            self.dataset_sampler = DatasetSampler(self.dataset, label_list=self.raw_labels)
            self.naive_sampler = get_cifar_100_naive(self.device)
            self.text_sampler = get_cifar_100_text(self.device)
            self.clip_sampler = get_cifar_100_clip(self.device)

        elif self.dataset_name == "cifar10":
            self.dataset = datasets.CIFAR10(root="datasets/cifar10", download=True, transform=tensorize_image_rgb(32))
            self.labels = cifar10_labels
            self.dataset_sampler = DatasetSampler(self.dataset, label_list=self.labels)
            self.naive_sampler = get_cifar_10_naive(self.device)
            self.text_sampler = get_cifar_10_text(self.device)
            self.clip_sampler = get_cifar_10_clip(self.device)

    def sample_list_of_text(self, labels, dataset=True):
        # Sample the images
        samples_store = {}
        for model, name in zip([self.dataset_sampler, self.naive_sampler, self.text_sampler, self.clip_sampler], ['dataset', 'naive', 'text', 'clip']):
            if dataset == False and name == 'dataset':
                continue
            print(f"Sampling from {name}")
            samples = model.sample_list_of_text(labels, progress=True)
            samples_store[name] = samples
        return samples_store
    
    def sample_batched_vector(self, vectors, dataset=True):
        # Sample the images
        samples_store = {}
        for model, name in zip([self.dataset_sampler, self.naive_sampler, self.text_sampler, self.clip_sampler], ['dataset', 'naive', 'text', 'clip']):
            if dataset == False and name == 'dataset':
                continue
            print(f"Sampling from {name}")
            samples = model.sample_batched_vector(vectors, progress=True)
            samples_store[name] = samples
        return samples_store
    
    def sample_all_labels(self, n=5):
        # Check if n is a integer larger than 0
        if not isinstance(n, int) or n <= 0:
            raise ValueError("n must be an integer larger than 0")
        labels = self.labels * n
        in_domain = self.sample_list_of_text(labels)
        data = {
            "labels": labels,
            "images": in_domain
        }
        return data

    def sample_mix_of_vectors(self, num_samples, num_labels):
        samples_store = {}
        with torch.no_grad():
            # For num_samples, choose num_labels
            # a.k.a., generate (num_samples, num_labels), where each number is a random integer chosen from the range of the number of labels
            labels = np.random.randint(0, len(self.labels), (num_samples, num_labels))
            # Now, we generate the mixing factors of the labels. We first generate random numbers of the same shape as labels
            mixing_factors = np.random.rand(*labels.shape)
            # We then normalize the mixing factors so that they sum to 1
            mixing_factors /= mixing_factors.sum(axis=1, keepdims=True)
            for model, name in zip([self.naive_sampler, self.text_sampler, self.clip_sampler], ['naive', 'text', 'clip']):
                # Now, for each model, we actually create the random samples
                # We first get the vectorized labels from each model
                vectorized_labels = []
                for label_set, mix_factor in zip(labels, mixing_factors):
                    # Make mix_factor broadcastable
                    mix_factor = np.expand_dims(mix_factor, 1)
                    vectorized_label_set = np.array(
                        [model.text_to_vector(self.labels[label]).cpu() for label in label_set]
                    )
                    # Now we mix the labels
                    vectorized_label = (vectorized_label_set * mix_factor).sum(axis=0)
                    vectorized_labels.append(vectorized_label)

                # Now, our vectorized labels are ready for sampling
                vectorized_labels = np.array(vectorized_labels)
                # Now, we sample the images
                print(f"Sampling from {name}")
                print(vectorized_labels.shape)
                # Convert to tensor on device
                vectorized_labels = torch.tensor(vectorized_labels).float().to(self.device)
                samples = model.sample_batched_vector(vectorized_labels, progress=True)
                samples_store[name] = samples
        return {
            "labels": labels,
            "mixing_factors": mixing_factors,
            "images": samples_store
        }

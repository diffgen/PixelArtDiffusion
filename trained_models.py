from diffusion.sampler import *
from diffusion.utils import plot_images
from diffusion.datasets import cifar10_labels, cifar100_labels

def get_cifar_10_naive(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar10_naive import get_models as get_naive_models
    naive_model = get_naive_models(device)

    # Load in the trained weights
    naive_model.load_state_dict(torch.load('final_models/cifar10_naive_ema.pth'))

    # Create sampler
    naive_sampler = Sampler(
        model=naive_model,
        label_list=cifar10_labels, # Used to lookup label names when you sample using text (as we don't have an encoder)
        device=device
    )

    return naive_sampler

def get_cifar_10_text(device="cuda"):
    from diffusion.train_scripts.cifar10_text import get_models as get_clip_models
    text_model, text_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    text_model.load_state_dict(torch.load('final_models/cifar10_text_ema.pth'))

    # Create sampler
    text_sampler = Sampler(
        model=text_model,
        text_embedder=text_embedding_model.encode,
        device=device
    )

    return text_sampler

def get_cifar_10_clip(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar10_clip import get_models as get_clip_models
    clip_model, clip_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    clip_model.load_state_dict(torch.load('final_models/cifar10_clip_ema.pth'))

    # Create sampler
    clip_sampler = Sampler(
        model=clip_model,
        text_embedder=clip_embedding_model.encode,
        device=device
    )

    return clip_sampler

def get_cifar_100_naive(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar100_naive import get_models as get_naive_models
    naive_model = get_naive_models(device)

    # Load in the trained weights
    naive_model.load_state_dict(torch.load('final_models/cifar100_naive_ema.pth'))

    # Create sampler
    naive_sampler = Sampler(
        model=naive_model,
        label_list=cifar100_labels, # Used to lookup label names when you sample using text (as we don't have an encoder)
        device=device
    )

    return naive_sampler

def get_cifar_100_text(device="cuda"):
    from diffusion.train_scripts.cifar100_text import get_models as get_clip_models
    text_model, text_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    text_model.load_state_dict(torch.load('final_models/cifar100_text_ema.pth'))

    # Create sampler
    text_sampler = Sampler(
        model=text_model,
        text_embedder=text_embedding_model.encode,
        device=device
    )

    return text_sampler

def get_cifar_100_clip(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar100_clip import get_models as get_clip_models
    clip_model, clip_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    clip_model.load_state_dict(torch.load('final_models/cifar100_clip_ema.pth'))

    # Create sampler
    clip_sampler = Sampler(
        model=clip_model,
        text_embedder=clip_embedding_model.encode,
        device=device
    )

    return clip_sampler

def get_cifar_10_clip_visual(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar10_clip_visual import get_models as get_clip_models
    clip_model, clip_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    clip_model.load_state_dict(torch.load('final_models/cifar10_clip_visual_ema.pth'))

    # Create sampler
    clip_sampler = Sampler(
        model=clip_model,
        text_embedder=clip_embedding_model.encode,
        device=device
    )

    return clip_sampler

def get_cifar_100_clip_visual(device="cuda"):
    # Get model and encoder
    from diffusion.train_scripts.cifar100_clip_visual import get_models as get_clip_models
    clip_model, clip_embedding_model, _ = get_clip_models(device)

    # Load in the trained weights
    clip_model.load_state_dict(torch.load('final_models/cifar100_clip_visual_ema.pth'))

    # Create sampler
    clip_sampler = Sampler(
        model=clip_model,
        text_embedder=clip_embedding_model.encode,
        device=device
    )

    return clip_sampler

def get_cifar_100_sampler():
    # Directly sample images from the CIFAR-100 dataset matching the label
    from diffusion.datasets import CIFAR100Dataset